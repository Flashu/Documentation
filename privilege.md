# Our Staff

The ModRealms Staff team is rapidly growing and is always adopting new members to help out on the network. The staff team is the core of the community and they make sure that your experience is as premium as it can be. All of the staff are dynamic however are assigned to a certain server that they applied for. They are always the go-to staff members incase your server needs help. [[Contacting the Staff]](https://modrealms.net/forum/index.php?threads/contacting-staff.120/).

### Owner
The Owner manages the network has a whole. This involves finance, which modpacks we will host a server for and decides the future direction of ModRealms. The Owner will have very close communication with the administration and management teams.

### Administration
Our Administrators handle the vast backend of the network. They have the ability to fix all issues that do not require direct SSH access. They will help with setting up new servers, general maintenance and assiting any managers who need help. They will be helping with promotions and dealing with firings of more senior staff members. This position reports directly to the Owner of the network.

### Management
On ModRealms, each Manager is assigned to their own server which they had worked as part of since the beginning. They handle the hiring/firing/promoting of the other staff that work on their server. They are the overseer of a certain server and are always the go-to staff member for said server if any major issues are to occur. They have the access to fix any issue for their assigned server that does not require any SSH access.

### Supervisor
Supervisors handle the issues that don't require any backend access. They can handle the issues that are not able to be resolved by a Moderator or below. They can help to deal with issues or bugs such as performance problems and questing issues. They are seen as a rolemodel to other staff and will always report directly to their Manager.

### Moderator
Moderators on the network have the ability to fix issues that perhaps a Helper cannot sort on their own. One of their main focuses includes engaging with other players on their server and ensuring that all issues they have are either resolved or escalated to a more senior staff member. Issues that a Moderator may need to deal with would include missing item issues, refunds and major chat issues.

## Helper
Our Helpers are the newest additions to the ModRealms staff team. This role for them serves as a trial before moving on to the Moderator role. This role helps the staff member adjust to the morals and ethics of becoming a staff member and to get to know the team. Helpers should act as a role model to all players on their server and should report any issues that they find to a more senior staff member on their server. The Helper should fight for a player until their issue is resolved.

## Developer
on ModRealms, Developers are given the same amount of permissions as a helper. This allows them to engage with the staff team incase they are given issues whilst there are no other staff members online. Developers help develop plugins for Sponge and BungeeCord that run on our Network. Developers are given tasks by the Owner and Administration team.