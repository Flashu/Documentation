# Welcome to the SkyClaims Wiki

SkyClaims is a plugin for SkyBlock servers that run [GriefPrevention](https://forums.spongepowered.org/t/griefprevention-official-thread/1123) for protection.
Instead of reinventing the wheel and adding custom protection for islands, SkyClaims will implement claims from one of Sponge's most powerful plugins, GriefPrevention!
With this design, nearly every GriefPrevention feature will be available to players for managing their islands.

This plugin is in Beta. Live support is available for the latest builds through [Discord](https://discord.gg/EkVQycV)!

## Features

- [X] Complete control of command usage with granular permissions (ie specific biomes for setbiome)
- [X] Automatic creation of islands and their encompassing claim
- [X] Support multiple island designs via Sponge schematics
- [X] Expanding islands using claim blocks
- [X] Isolation of islands to their own Minecraft region file
- [X] Allow spawn/tp on your island at a configurable location
- [X] Work in teams:
    - [X] Invite new players to an island
    - [X] Kick existing players from an island
    - [X] Leave an island
    - [X] Transfer island ownership to another player
    - [X] Limit the number of islands a player may join
- [X] Change the biome of a block, chunk or entire island
- [X] Limit entity spawning per island
- [X] Automatic removal of inactive islands
- [ ] Configurable island layouts (linear & spiral planned)
- [ ] Island messaging channel for chatting within your island
- [ ] Economy support for schematics & island expansion

## Dependencies

- **Required:**
   - [Sponge](https://www.spongepowered.org/downloads) - 1.12.2-2555-7.0.0-BETA-2800+
   - [Grief Prevention](https://forums.spongepowered.org/t/griefprevention-official-thread/1123) - 1.12.2-4.3.0.509+
   - Permission Plugin - [LuckPerms](https://forums.spongepowered.org/t/luckperms-an-advanced-permissions-plugin/14274) is highly recommended.
- **Optional:**
   - [Nucleus](https://nucleuspowered.org) - 1.2.0+
   <!-- - Economy Plugin - [Economy Lite](https://ore.spongepowered.org/Flibio/EconomyLite), [Total Economy](https://ore.spongepowered.org/Erigitic/Total-Economy), or any other Sponge Economy plugin of your choosing. -->

## Downloads

**Plugin releases are available on [Ore](https://ore.spongepowered.org/Mohron/SkyClaims/).** 

Make sure to download the file matching your server's SpongeAPI version: **S5** for API 5/6 and **S7** for API 7.

## Additional Information

**Bug reports and feature requests can be made on [GitHub](https://github.com/DevOnTheRocks/SkyClaims/issues).**

[![Discord](_images/Discord.png)](https://discord.gg/EkVQycV)
| [![PayPal](_images/Paypal.png)](https://www.paypal.me/mohron)
