## Supported Biome Types

Starting with build 27.1 for Sponge API 7 only, modded biomes should be supported in addition to [Vanilla](#vanilla) biomes.  The biome lists below are just for quick reference and may not be kept up to date. To generate a list of biomes enable `Misc > Log-Biomes` in your config.

### Minecraft

| Biome Name | Biome ID | Biome Permission |
| ---------- | -------- | ---------------- |
| Beach | `minecraft:beaches` | `skyclaims.arguments.biomes.minecraft.beaches` |
| Birch Forest | `minecraft:birch_forest` | `skyclaims.arguments.biomes.minecraft.birch_forest` |
| Birch Forest Hills | `minecraft:birch_forest_hills` | `skyclaims.arguments.biomes.minecraft.birch_forest_hills` |
| Cold Beach | `minecraft:cold_beach` | `skyclaims.arguments.biomes.minecraft.cold_beach` |
| Deep Ocean | `minecraft:deep_ocean` | `skyclaims.arguments.biomes.minecraft.deep_ocean` |
| Desert | `minecraft:desert` | `skyclaims.arguments.biomes.minecraft.desert` |
| DesertHills | `minecraft:desert_hills` | `skyclaims.arguments.biomes.minecraft.desert_hills` |
| Extreme Hills | `minecraft:extreme_hills` | `skyclaims.arguments.biomes.minecraft.extreme_hills` |
| Extreme Hills+ | `minecraft:extreme_hills_with_trees` | `skyclaims.arguments.biomes.minecraft.extreme_hills_with_trees` |
| Forest | `minecraft:forest` | `skyclaims.arguments.biomes.minecraft.forest` |
| ForestHills | `minecraft:forest_hills` | `skyclaims.arguments.biomes.minecraft.forest_hills` |
| FrozenOcean | `minecraft:frozen_ocean` | `skyclaims.arguments.biomes.minecraft.frozen_ocean` |
| FrozenRiver | `minecraft:frozen_river` | `skyclaims.arguments.biomes.minecraft.frozen_river` |
| Hell | `minecraft:hell` | `skyclaims.arguments.biomes.minecraft.hell` |
| Ice Plains | `minecraft:ice_flats` | `skyclaims.arguments.biomes.minecraft.ice_flats` |
| Ice Mountains | `minecraft:ice_mountains` | `skyclaims.arguments.biomes.minecraft.ice_mountains` |
| Jungle | `minecraft:jungle` | `skyclaims.arguments.biomes.minecraft.jungle` |
| JungleEdge | `minecraft:jungle_edge` | `skyclaims.arguments.biomes.minecraft.jungle_edge` |
| JungleHills | `minecraft:jungle_hills` | `skyclaims.arguments.biomes.minecraft.jungle_hills` |
| Mesa | `minecraft:mesa` | `skyclaims.arguments.biomes.minecraft.mesa` |
| Mesa Plateau | `minecraft:mesa_clear_rock` | `skyclaims.arguments.biomes.minecraft.mesa_clear_rock` |
| Mesa Plateau F | `minecraft:mesa_rock` | `skyclaims.arguments.biomes.minecraft.mesa_rock` |
| MushroomIsland | `minecraft:mushroom_island` | `skyclaims.arguments.biomes.minecraft.mushroom_island` |
| MushroomIslandShore | `minecraft:mushroom_island_shore` | `skyclaims.arguments.biomes.minecraft.mushroom_island_shore` |
| Birch Forest M | `minecraft:mutated_birch_forest` | `skyclaims.arguments.biomes.minecraft.mutated_birch_forest` |
| Birch Forest Hills M | `minecraft:mutated_birch_forest_hills` | `skyclaims.arguments.biomes.minecraft.mutated_birch_forest_hills` |
| Desert M | `minecraft:mutated_desert` | `skyclaims.arguments.biomes.minecraft.mutated_desert` |
| Extreme Hills M | `minecraft:mutated_extreme_hills` | `skyclaims.arguments.biomes.minecraft.mutated_extreme_hills` |
| Extreme Hills+ M | `minecraft:mutated_extreme_hills_with_trees` | `skyclaims.arguments.biomes.minecraft.mutated_extreme_hills_with_trees` |
| Flower Forest | `minecraft:mutated_forest` | `skyclaims.arguments.biomes.minecraft.mutated_forest` |
| Ice Plains Spikes | `minecraft:mutated_ice_flats` | `skyclaims.arguments.biomes.minecraft.mutated_ice_flats` |
| Jungle M | `minecraft:mutated_jungle` | `skyclaims.arguments.biomes.minecraft.mutated_jungle` |
| JungleEdge M | `minecraft:mutated_jungle_edge` | `skyclaims.arguments.biomes.minecraft.mutated_jungle_edge` |
| Mesa (Bryce) | `minecraft:mutated_mesa` | `skyclaims.arguments.biomes.minecraft.mutated_mesa` |
| Mesa Plateau M | `minecraft:mutated_mesa_clear_rock` | `skyclaims.arguments.biomes.minecraft.mutated_mesa_clear_rock` |
| Mesa Plateau F M | `minecraft:mutated_mesa_rock` | `skyclaims.arguments.biomes.minecraft.mutated_mesa_rock` |
| Sunflower Plains | `minecraft:mutated_plains` | `skyclaims.arguments.biomes.minecraft.mutated_plains` |
| Mega Spruce Taiga | `minecraft:mutated_redwood_taiga` | `skyclaims.arguments.biomes.minecraft.mutated_redwood_taiga` |
| Redwood Taiga Hills M | `minecraft:mutated_redwood_taiga_hills` | `skyclaims.arguments.biomes.minecraft.mutated_redwood_taiga_hills` |
| Roofed Forest M | `minecraft:mutated_roofed_forest` | `skyclaims.arguments.biomes.minecraft.mutated_roofed_forest` |
| Savanna M | `minecraft:mutated_savanna` | `skyclaims.arguments.biomes.minecraft.mutated_savanna` |
| Savanna Plateau M | `minecraft:mutated_savanna_rock` | `skyclaims.arguments.biomes.minecraft.mutated_savanna_rock` |
| Swampland M | `minecraft:mutated_swampland` | `skyclaims.arguments.biomes.minecraft.mutated_swampland` |
| Taiga M | `minecraft:mutated_taiga` | `skyclaims.arguments.biomes.minecraft.mutated_taiga` |
| Cold Taiga M | `minecraft:mutated_taiga_cold` | `skyclaims.arguments.biomes.minecraft.mutated_taiga_cold` |
| Ocean | `minecraft:ocean` | `skyclaims.arguments.biomes.minecraft.ocean` |
| Plains | `minecraft:plains` | `skyclaims.arguments.biomes.minecraft.plains` |
| Mega Taiga | `minecraft:redwood_taiga` | `skyclaims.arguments.biomes.minecraft.redwood_taiga` |
| Mega Taiga Hills | `minecraft:redwood_taiga_hills` | `skyclaims.arguments.biomes.minecraft.redwood_taiga_hills` |
| River | `minecraft:river` | `skyclaims.arguments.biomes.minecraft.river` |
| Roofed Forest | `minecraft:roofed_forest` | `skyclaims.arguments.biomes.minecraft.roofed_forest` |
| Savanna | `minecraft:savanna` | `skyclaims.arguments.biomes.minecraft.savanna` |
| Savanna Plateau | `minecraft:savanna_rock` | `skyclaims.arguments.biomes.minecraft.savanna_rock` |
| The End | `minecraft:sky` | `skyclaims.arguments.biomes.minecraft.sky` |
| Extreme Hills Edge | `minecraft:smaller_extreme_hills` | `skyclaims.arguments.biomes.minecraft.smaller_extreme_hills` |
| Stone Beach | `minecraft:stone_beach` | `skyclaims.arguments.biomes.minecraft.stone_beach` |
| Swampland | `minecraft:swampland` | `skyclaims.arguments.biomes.minecraft.swampland` |
| Taiga | `minecraft:taiga` | `skyclaims.arguments.biomes.minecraft.taiga` |
| Cold Taiga | `minecraft:taiga_cold` | `skyclaims.arguments.biomes.minecraft.taiga_cold` |
| Cold Taiga Hills | `minecraft:taiga_cold_hills` | `skyclaims.arguments.biomes.minecraft.taiga_cold_hills` |
| TaigaHills | `minecraft:taiga_hills` | `skyclaims.arguments.biomes.minecraft.taiga_hills` |
| The Void | `minecraft:void` | `skyclaims.arguments.biomes.minecraft.void` |

### Biomes O' Plenty

| Biome Name | Biome ID | Biome Permission |
| ---------- | -------- | ---------------- |
| Alps | `biomesoplenty:alps` | `skyclaims.arguments.biomes.biomesoplenty.alps` |
| AlpsFoothills | `biomesoplenty:alps_foothills` | `skyclaims.arguments.biomes.biomesoplenty.alps_foothills` |
| Bamboo Forest | `biomesoplenty:bamboo_forest` | `skyclaims.arguments.biomes.biomesoplenty.bamboo_forest` |
| Bayou | `biomesoplenty:bayou` | `skyclaims.arguments.biomes.biomesoplenty.bayou` |
| Bog | `biomesoplenty:bog` | `skyclaims.arguments.biomes.biomesoplenty.bog` |
| Boreal Forest | `biomesoplenty:boreal_forest` | `skyclaims.arguments.biomes.biomesoplenty.boreal_forest` |
| Brushland | `biomesoplenty:brushland` | `skyclaims.arguments.biomes.biomesoplenty.brushland` |
| Chaparral | `biomesoplenty:chaparral` | `skyclaims.arguments.biomes.biomesoplenty.chaparral` |
| Cherry Blossom Grove | `biomesoplenty:cherry_blossom_grove` | `skyclaims.arguments.biomes.biomesoplenty.cherry_blossom_grove` |
| Cold Desert | `biomesoplenty:cold_desert` | `skyclaims.arguments.biomes.biomesoplenty.cold_desert` |
| Coniferous Forest | `biomesoplenty:coniferous_forest` | `skyclaims.arguments.biomes.biomesoplenty.coniferous_forest` |
| Coral Reef | `biomesoplenty:coral_reef` | `skyclaims.arguments.biomes.biomesoplenty.coral_reef` |
| Corrupted Sands | `biomesoplenty:corrupted_sands` | `skyclaims.arguments.biomes.biomesoplenty.corrupted_sands` |
| Crag | `biomesoplenty:crag` | `skyclaims.arguments.biomes.biomesoplenty.crag` |
| Dead Forest | `biomesoplenty:dead_forest` | `skyclaims.arguments.biomes.biomesoplenty.dead_forest` |
| Dead Swamp | `biomesoplenty:dead_swamp` | `skyclaims.arguments.biomes.biomesoplenty.dead_swamp` |
| Eucalyptus Forest | `biomesoplenty:eucalyptus_forest` | `skyclaims.arguments.biomes.biomesoplenty.eucalyptus_forest` |
| Fen | `biomesoplenty:fen` | `skyclaims.arguments.biomes.biomesoplenty.fen` |
| Flower Field | `biomesoplenty:flower_field` | `skyclaims.arguments.biomes.biomesoplenty.flower_field` |
| Flower Island | `biomesoplenty:flower_island` | `skyclaims.arguments.biomes.biomesoplenty.flower_island` |
| Fungi Forest | `biomesoplenty:fungi_forest` | `skyclaims.arguments.biomes.biomesoplenty.fungi_forest` |
| Glacier | `biomesoplenty:glacier` | `skyclaims.arguments.biomes.biomesoplenty.glacier` |
| Grassland | `biomesoplenty:grassland` | `skyclaims.arguments.biomes.biomesoplenty.grassland` |
| Gravel Beach | `biomesoplenty:gravel_beach` | `skyclaims.arguments.biomes.biomesoplenty.gravel_beach` |
| Grove | `biomesoplenty:grove` | `skyclaims.arguments.biomes.biomesoplenty.grove` |
| Highland | `biomesoplenty:highland` | `skyclaims.arguments.biomes.biomesoplenty.highland` |
| Kelp Forest | `biomesoplenty:kelp_forest` | `skyclaims.arguments.biomes.biomesoplenty.kelp_forest` |
| Land of Lakes | `biomesoplenty:land_of_lakes` | `skyclaims.arguments.biomes.biomesoplenty.land_of_lakes` |
| Lavender Fields | `biomesoplenty:lavender_fields` | `skyclaims.arguments.biomes.biomesoplenty.lavender_fields` |
| Lush Desert | `biomesoplenty:lush_desert` | `skyclaims.arguments.biomes.biomesoplenty.lush_desert` |
| Lush Swamp | `biomesoplenty:lush_swamp` | `skyclaims.arguments.biomes.biomesoplenty.lush_swamp` |
| Mangrove | `biomesoplenty:mangrove` | `skyclaims.arguments.biomes.biomesoplenty.mangrove` |
| Maple Woods | `biomesoplenty:maple_woods` | `skyclaims.arguments.biomes.biomesoplenty.maple_woods` |
| Marsh | `biomesoplenty:marsh` | `skyclaims.arguments.biomes.biomesoplenty.marsh` |
| Meadow | `biomesoplenty:meadow` | `skyclaims.arguments.biomes.biomesoplenty.meadow` |
| Moor | `biomesoplenty:moor` | `skyclaims.arguments.biomes.biomesoplenty.moor` |
| Mountain | `biomesoplenty:mountain` | `skyclaims.arguments.biomes.biomesoplenty.mountain` |
| MountainFoothills | `biomesoplenty:mountain_foothills` | `skyclaims.arguments.biomes.biomesoplenty.mountain_foothills` |
| Mystic Grove | `biomesoplenty:mystic_grove` | `skyclaims.arguments.biomes.biomesoplenty.mystic_grove` |
| Oasis | `biomesoplenty:oasis` | `skyclaims.arguments.biomes.biomesoplenty.oasis` |
| Ominous Woods | `biomesoplenty:ominous_woods` | `skyclaims.arguments.biomes.biomesoplenty.ominous_woods` |
| Orchard | `biomesoplenty:orchard` | `skyclaims.arguments.biomes.biomesoplenty.orchard` |
| Origin Island | `biomesoplenty:origin_island` | `skyclaims.arguments.biomes.biomesoplenty.origin_island` |
| Outback | `biomesoplenty:outback` | `skyclaims.arguments.biomes.biomesoplenty.outback` |
| Overgrown Cliffs | `biomesoplenty:overgrown_cliffs` | `skyclaims.arguments.biomes.biomesoplenty.overgrown_cliffs` |
| Phantasmagoric Inferno | `biomesoplenty:phantasmagoric_inferno` | `skyclaims.arguments.biomes.biomesoplenty.phantasmagoric_inferno` |
| Polar Chasm | `biomesoplenty:polar_chasm` | `skyclaims.arguments.biomes.biomesoplenty.polar_chasm` |
| Prairie | `biomesoplenty:prairie` | `skyclaims.arguments.biomes.biomesoplenty.prairie` |
| Quagmire | `biomesoplenty:quagmire` | `skyclaims.arguments.biomes.biomesoplenty.quagmire` |
| Rainforest | `biomesoplenty:rainforest` | `skyclaims.arguments.biomes.biomesoplenty.rainforest` |
| Redwood Forest | `biomesoplenty:redwood_forest` | `skyclaims.arguments.biomes.biomesoplenty.redwood_forest` |
| Sacred Springs | `biomesoplenty:sacred_springs` | `skyclaims.arguments.biomes.biomesoplenty.sacred_springs` |
| Seasonal Forest | `biomesoplenty:seasonal_forest` | `skyclaims.arguments.biomes.biomesoplenty.seasonal_forest` |
| Shield | `biomesoplenty:shield` | `skyclaims.arguments.biomes.biomesoplenty.shield` |
| Shrubland | `biomesoplenty:shrubland` | `skyclaims.arguments.biomes.biomesoplenty.shrubland` |
| Snowy Coniferous Forest | `biomesoplenty:snowy_coniferous_forest` | `skyclaims.arguments.biomes.biomesoplenty.snowy_coniferous_forest` |
| Snowy Forest | `biomesoplenty:snowy_forest` | `skyclaims.arguments.biomes.biomesoplenty.snowy_forest` |
| Steppe | `biomesoplenty:steppe` | `skyclaims.arguments.biomes.biomesoplenty.steppe` |
| Temperate Rainforest | `biomesoplenty:temperate_rainforest` | `skyclaims.arguments.biomes.biomesoplenty.temperate_rainforest` |
| Tropical Island | `biomesoplenty:tropical_island` | `skyclaims.arguments.biomes.biomesoplenty.tropical_island` |
| Tropical Rainforest | `biomesoplenty:tropical_rainforest` | `skyclaims.arguments.biomes.biomesoplenty.tropical_rainforest` |
| Tundra | `biomesoplenty:tundra` | `skyclaims.arguments.biomes.biomesoplenty.tundra` |
| Undergarden | `biomesoplenty:undergarden` | `skyclaims.arguments.biomes.biomesoplenty.undergarden` |
| Visceral Heap | `biomesoplenty:visceral_heap` | `skyclaims.arguments.biomes.biomesoplenty.visceral_heap` |
| Volcanic Island | `biomesoplenty:volcanic_island` | `skyclaims.arguments.biomes.biomesoplenty.volcanic_island` |
| Wasteland | `biomesoplenty:wasteland` | `skyclaims.arguments.biomes.biomesoplenty.wasteland` |
| Wetland | `biomesoplenty:wetland` | `skyclaims.arguments.biomes.biomesoplenty.wetland` |
| White Beach | `biomesoplenty:white_beach` | `skyclaims.arguments.biomes.biomesoplenty.white_beach` |
| Woodland | `biomesoplenty:woodland` | `skyclaims.arguments.biomes.biomesoplenty.woodland` |
| Xeric Shrubland | `biomesoplenty:xeric_shrubland` | `skyclaims.arguments.biomes.biomesoplenty.xeric_shrubland` |

### Nether Ex

| Biome Name | Biome ID | Biome Permission |
| ---------- | -------- | ---------------- |
| Arctic Abyss | `nex:arctic_abyss` | `skyclaims.arguments.biomes.nex.arctic_abyss` |
| Fungi Forest | `nex:fungi_forest` | `skyclaims.arguments.biomes.nex.fungi_forest` |
| Hell | `nex:hell` | `skyclaims.arguments.biomes.nex.hell` |
| Ruthless Sands | `nex:ruthless_sands` | `skyclaims.arguments.biomes.nex.ruthless_sands` |
| Torrid Wasteland | `nex:torrid_wasteland` | `skyclaims.arguments.biomes.nex.torrid_wasteland` |
