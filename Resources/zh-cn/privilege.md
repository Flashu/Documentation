# 阶级系统
<!--
    在GP里面终于有阶级管理了，你们这些屁民。屈服吧！
                           --来自被无数次熊掉空岛的狂躁的subtank
-->
## 介绍
SkyClaims提供了一个很好的阶级系统来管理每个玩家对一个空岛的修改权限（命令上的）。阶级系统是在大概B25版本添加的。
GriefPrevention's [trust types](https://github.com/MinecraftPortCentral/GriefPrevention/wiki/Trust-System).

## 等级
**Note：Because the ingame text was not translation, so keep the keyword for check**<br />
**注意：因为插件文本未汉化，因此中文部分保留中文和英文两种关键词，以便核对**
- 拥有者（Owner）
    - 最高的等级权限。
    - 一个空岛只有一个拥有者。
    - 等价于GP中的区域所有者。
    - 能够使用所有能使用的空岛命令
- 管理者（Manager）
    - 只能由拥有者授予该等级的权限。
    - 等价于GP中的管理者。
    - 拥有几乎所有的权限，但是不能移除或者重置岛屿。
- 参与者（Member）
<!--
苦力？希望不是苦力怕.   ←_← 第一次玩梗玩的这么一致。
coolie?hopely not a creeper.  ←_←  it is first time for playing joke so same.
-->
    - 可以被拥有者和参与者授予该阶级。
    - 等价于GP中的建设者权限。
    - 仅仅有最最基础的空岛权限，不能使用任何相关命令或者修改任何空岛的特征。
- 游客（None）
    - 默认的阶级等级
    - 仅仅能够访问未上锁的空岛或者直接传送过去。
    - 不能对任何方块进行修改。
