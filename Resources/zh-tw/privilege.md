﻿# 階級系統
<!--
    在GP裡面終於有階級管理了，你們這些屁民。屈服吧！
                           --來自被無數次熊掉空島的狂躁的subtank
-->
## 介紹
SkyClaims提供了一個很好的階級系統來管理每個玩家對一個空島的修改權限（命令上的）。階級系統是在大概B25版本添加的。
GriefPrevention's [trust types](https://github.com/MinecraftPortCentral/GriefPrevention/wiki/Trust-System).

## 等級
**Note：Because the ingame text was not translation, so keep the keyword for check**
**注意：因為插件文本未漢化，因此中文部分保留中文和英文兩種關鍵詞，以便核對**
- 擁有者（Owner）
    - 最高的等級權限。
    - 一個空島只有一個擁有者。
    - 等價於GP中的區域所有者。
    - 能夠使用所有能使用的空島命令
- 管理者（Manager）
    - 只能由擁有者授予該等級的權限。
    - 等價於GP中的管理者。
    - 擁有幾乎所有的權限，但是不能移除或者重置島嶼。
- 參與者（Member）
<!--
苦力？希望不是苦力怕. ←_← 第一次玩梗玩的這麼一致。
coolie?hopely not a creeper. ←_← it is first time for playing joke so same.
-->
    - 可以被擁有者和參與者授予該階級。
    - 等價於GP中的建設者權限。
    - 僅僅有最最基礎的空島權限，不能使用任何相關命令或者修改任何空島的特徵。
- 遊客（None）
    - 默認的階級等級
    - 僅僅能夠訪問未上鎖的空島或者直接傳送過去。
    - 不能對任何方塊進行修改。
