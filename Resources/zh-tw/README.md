﻿# 歡迎來到SkyClaims的Wiki頁面

SkyClaims是一個運行在Sponge服務端並且依賴於[GriefPrevention](https://forums.spongepowered.org/t/griefprevention-official-thread/1123)保護的空島服務器插件。 GriefPrevention是一款在Sponge上面非常好的領地保護插件，玩家可以在空島上面使用GriefPrevention提供的幾乎所有功能。

 本插件還在beta階段，最新版本的支持在這裡[Discord](https://discord.gg/EkVQycV)！

## 功能

- [X] 完整而詳盡的命令權限。 （例如：利用/setbiome定義特定的生物群系）
- [X] 自動創建空島以及周圍領地範圍
- [X] 利用Sponge的schematics文件支持多種預設的空島模板
- [X] 可擴展空島領地範圍（需要付出一些資源/金錢需要經濟插件支持）
- [X] 可單獨創建一個空島世界與其他世界分離
- [X] 自定義島嶼重生點或者tp點
- [X] 團隊功能：
    - [X] 邀請朋友到島上
    - [X] 踢出一個在島上的玩家
    - [X] 離開一個島
    - [X] 轉贈一個島嶼(移交財產麼?2333)
    - [ ] 限制玩家加入的島嶼數量
- [X] 可自定義修改生物群系，小到一個方塊，大到你的全島，隨你定義
- [X] 可以定義限制刷怪機制
- [X] 自動移除非活躍島嶼
- [ ] 可配置的島嶼佈局格式 (線性或是螺旋狀分佈島嶼，詳見wiki部分)
- [ ] 添加島嶼私聊頻道方便隊伍聊天
- [ ] 經濟插件的支持

## 依賴插件

- **必須插件**
   - [Sponge](https://www.spongepowered.org/downloads) - 1.10.2-2281-5.2.0-BETA-2558+
   - [Grief Prevention](https://forums.spongepowered.org/t/griefprevention-official-thread/1123) - 1.10.2-4.0.0.415+
   - Permission Plugin - 推薦使用[LuckPerms](https://forums.spongepowered.org/t/luckperms-an-advanced-permissions-plugin/14274)
- **可選插件**
   - [Nucleus](https://nucleuspowered.org) - 1.1.3-LTS+
   <!-- - Economy Plugin - [Economy Lite](https://ore.spongepowered.org/Flibio/EconomyLite), [Total Economy](https://ore.spongepowered.org/Erigitic/Total-Economy), or any other Sponge Economy plugin of your choosing. -->

## 附加信息

**插件發布地址[Ore](https://ore.spongepowered.org/Mohron/SkyClaims/)。 **

**Bug提交和功能建議可以發到這裡[GitHub](https://github.com/DevOnTheRocks/SkyClaims/issues)。 **

[![Discord](/_images/Discord.png)](https://discord.gg/EkVQycV)
| [![PayPal](/_images/Paypal.png)](https://www.paypal.me/mohron)
